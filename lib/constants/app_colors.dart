import 'dart:ui';

class AppColors {
  static const primary = Color(0xFF22A2BD);
  static const background = Color(0xFFFFFFFF);
  static const more1 = Color(0xFF27AE60);
  static const more2 = Color(0xFFEB5757);
  static const mainText = Color(0xFF0B1E2D);
  static const neutral1 = Color(0xFFF2F2F2);
  static const neutral2 = Color(0xFF5B6975);
  static const under = Color(0xFF828282);
  static const hint = Color(0xFFBDBDBD);
  static const neutral3 = Color(0xFFBDBDBD);
  static const splashBackground = Color(0xFF091824);
}
