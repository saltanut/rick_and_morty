/*import 'package:flutter/material.dart';
import 'package:rick_and_morty/ui/characters_list/characters_list_screen.dart';
import 'package:rick_and_morty/ui/login_screen/widgets/login_field.dart';

import '../../../generated/l10n.dart';
import '../../constants/app_assets.dart';
import '../../constants/app_colors.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  String login = "";
  String password = "";

  void validate() {
    FocusScope.of(context).unfocus();

    if (formkey.currentState!.validate()) {
      if (login != "qwerty" || password != "123456ab") {
        showAlertDialog(context);
      } else {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => const CharactersList(),
          ),
        );
      }
    }
  }

  showAlertDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(S.of(context).tryAgain,
              style: const TextStyle(fontSize: 25)),
          actions: <Widget>[
            ElevatedButton(
              onPressed: () => Navigator.pop(context, 'S.of(context).close'),
              child: Text(S.of(context).close),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Spacer(),
            Image.asset(AppAssets.images.logo),
            Text(
              S.of(context).inputLoginAndPassword,
              style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            LoginTextField(),
            /*
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Form(
                key: formkey,
                child: Column(children: <Widget>[
                  TextFormField(
                    decoration: InputDecoration(
                      border: const UnderlineInputBorder(),
                      labelText: S.of(context).login,
                      counterText: '${login.length.toString()}/8',
                      counterStyle: const TextStyle(
                        fontSize: 15,
                      ),
                      prefixIcon: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: SvgPicture.asset(AppAssets.svg.account,
                            width: 16.0, color: AppColors.neutral2),
                      ),
                    ),
                    onChanged: (value) {
                      setState(() {
                        login = value;
                      });
                    },
                    style: const TextStyle(fontSize: 20),
                    validator: (value) {
                      if (value == null) {
                        return S.of(context).inputErrorCheckLogin;
                      }
                      if (value.length < 3) {
                        return S.of(context).inputErrorLoginIsShort;
                      } else {
                        return null;
                      }
                    },
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    obscureText: true,
                    decoration: InputDecoration(
                      border: const UnderlineInputBorder(),
                      labelText: S.of(context).password,
                      counterText: '${password.length}/16',
                      counterStyle: const TextStyle(
                        fontSize: 15,
                      ),
                      prefixIcon: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: SvgPicture.asset(AppAssets.svg.password,
                            width: 16.0, color: AppColors.neutral2),
                      ),
                    ),
                    onChanged: (value) {
                      setState(() {
                        password = value;
                      });
                    },
                    style: const TextStyle(fontSize: 20),
                    validator: (String? value) {
                      if (value == null) {
                        return S.of(context).inputErrorCheckPassword;
                      }
                      if (value.length < 8) {
                        return S.of(context).inputErrorPasswordIsShort;
                      } else {
                        return null;
                      }
                    },
                  ),
                ]),
              ),
            ),*/
            const Spacer(),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  minimumSize: const Size.fromHeight(50),
                ),
                onPressed: validate,
                child: Text(
                  S.of(context).signIn,
                  style: const TextStyle(fontSize: 18),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  S.of(context).dontHaveAnAccountHint,
                  style: const TextStyle(
                    fontSize: 16,
                    color: AppColors.neutral2,
                  ),
                ),
                Text(
                  S.of(context).create,
                  style: const TextStyle(
                    fontSize: 16,
                    color: AppColors.neutral2,
                  ),
                ),
              ],
            ),
            const Spacer(),
          ],
        ),
      ),
    );
  }
}
*/
import 'package:flutter/material.dart';
import 'package:rick_and_morty/constants/app_assets.dart';
import 'package:rick_and_morty/constants/app_colors.dart';
import 'package:rick_and_morty/constants/app_styles.dart';
import 'package:rick_and_morty/ui/characters_list/characters_list_screen.dart';
import 'package:rick_and_morty/widgets/app_alert_dialog.dart';
import 'package:rick_and_morty/widgets/app_button_styles.dart';

import '../../generated/l10n.dart';
import 'widgets/login_field.dart';
import 'widgets/password_field.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final formKey = GlobalKey<FormState>();

  String? login;
  String? password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.background,
      body: Column(
        children: [
          Expanded(
            child: Image.asset(AppAssets.images.logo),
          ),
          Padding(
            padding: const EdgeInsets.all(24.0),
            child: Column(
              children: [
                Form(
                  key: formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        S.of(context).login,
                        style: AppStyles.s14w400.copyWith(
                          height: 2.0,
                          leadingDistribution: TextLeadingDistribution.even,
                        ),
                        textAlign: TextAlign.start,
                      ),
                      LoginTextField(
                        onSaved: (login) {
                          this.login = login;
                        },
                      ),
                      const SizedBox(height: 10.0),
                      Text(
                        S.of(context).password,
                        style: AppStyles.s14w400.copyWith(
                          height: 2.0,
                          leadingDistribution: TextLeadingDistribution.even,
                        ),
                        textAlign: TextAlign.start,
                      ),
                      PasswordTextField(
                        onSaved: (value) {
                          password = value;
                        },
                      ),
                      const SizedBox(height: 24.0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Expanded(
                            child: ElevatedButton(
                              style: AppButtonStyles.elevated1,
                              child: Text(S.of(context).signIn),
                              onPressed: () {
                                final isValidated =
                                    formKey.currentState?.validate() ?? false;
                                if (isValidated) {
                                  FocusScope.of(context).unfocus();
                                  formKey.currentState?.save();
                                  if (login == 'qwerty' &&
                                      password == '123456ab') {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                            const CharactersListScreen(),
                                      ),
                                    );
                                  } else {
                                    showDialog(
                                      context: context,
                                      builder: (context) {
                                        return AppAlertDialog(
                                          title: Text(S.of(context).error),
                                          content: Text(
                                            S.of(context).wrongLoginOrPassword,
                                          ),
                                        );
                                      },
                                    );
                                  }
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 10.0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Flexible(
                            child:
                                Text('${S.of(context).dontHaveAnAccountHint}?'),
                          ),
                          TextButton(
                            style: AppButtonStyles.text1,
                            child: Text(
                              S.of(context).create,
                            ),
                            onPressed: () {},
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
