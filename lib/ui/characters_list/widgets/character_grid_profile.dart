import 'package:rick_and_morty/constants/app_colors.dart';
import 'package:rick_and_morty/constants/app_styles.dart';
import 'package:rick_and_morty/dto/character.dart';
import 'package:rick_and_morty/generated/l10n.dart';
import 'package:rick_and_morty/widgets/user_avatar.dart';
import 'package:flutter/material.dart';

class CharacterGridProfile extends StatelessWidget {
  const CharacterGridProfile(this.character, {Key? key}) : super(key: key);

  final Character character;

  String _statusLabel(String? status) {
    if (status == 'Dead') return S.current.dead;
    if (status == 'Alive') return S.current.alive;
    return S.current.noData;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        UserAvatar(
          character.image,
          radius: 60.0,
          margin: const EdgeInsets.only(bottom: 20.0),
        ),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                children: [
                  Expanded(
                    child: Text(
                      _statusLabel(character.status).toUpperCase(),
                      style: AppStyles.s10w500.copyWith(
                        letterSpacing: 1.5,
                        color: (character.status == 'Dead')
                            ? Colors.red
                            : (character.status == 'Alive')
                                ? const Color(0xff00c48c)
                                : Colors.grey,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      character.name ?? S.of(context).noData,
                      textAlign: TextAlign.center,
                      style: AppStyles.s16w500,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      '${character.species ?? S.of(context).noData}, ${character.gender ?? S.of(context).noData}',
                      style: const TextStyle(
                        color: AppColors.neutral2,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              )
            ],
          ),
        )
      ],
    );
  }
}
