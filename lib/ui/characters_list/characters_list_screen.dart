import 'package:flutter/material.dart';
import 'package:rick_and_morty/constants/app_colors.dart';
import 'package:rick_and_morty/constants/app_styles.dart';
import 'package:rick_and_morty/dto/character.dart';
import 'package:rick_and_morty/generated/l10n.dart';
import 'package:rick_and_morty/repo/repo_characters.dart';
import 'package:rick_and_morty/ui/characters_list/widgets/vmodel.dart';
import 'package:rick_and_morty/widgets/app_nav_bar.dart';
import 'package:provider/provider.dart';

import 'widgets/character_grid_profile.dart';
import 'widgets/search_bar.dart';
import 'widgets/character_list_profile.dart';

part 'widgets/list_view.dart';
part 'widgets/grid_view.dart';

class CharactersListScreen extends StatelessWidget {
  const CharactersListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: const AppNavBar(current: 0),
        body: ChangeNotifierProvider(
          create: (context) => CharactersListVModel(
            repo: Provider.of<RepoCharacters>(context, listen: false),
          ),
          builder: (context, _) {
            final charactersTotal =
                context.watch<CharactersListVModel>().filteredList.length;
            return Column(
              children: [
                SearchBar(
                  onChanged: (value) {
                    Provider.of<CharactersListVModel>(context, listen: false)
                        .filter(
                      value.toLowerCase(),
                    );
                  },
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: Text(
                          S
                              .of(context)
                              .charactersTotal(charactersTotal)
                              .toUpperCase(),
                          style: AppStyles.s10w500.copyWith(
                            letterSpacing: 1.5,
                            color: AppColors.neutral2,
                          ),
                        ),
                      ),
                      IconButton(
                        icon: const Icon(Icons.grid_view),
                        iconSize: 28.0,
                        color: AppColors.neutral2,
                        onPressed: () {
                          Provider.of<CharactersListVModel>(
                            context,
                            listen: false,
                          ).switchView();
                        },
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Consumer<CharactersListVModel>(
                    builder: (context, vmodel, _) {
                      if (vmodel.isLoading) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            CircularProgressIndicator(),
                          ],
                        );
                      }
                      if (vmodel.errorMessage != null) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Flexible(
                              child: Text(vmodel.errorMessage!),
                            ),
                          ],
                        );
                      }
                      if (vmodel.filteredList.isEmpty) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Flexible(
                              child: Text(S.of(context).charactersListIsEmpty),
                            ),
                          ],
                        );
                      }
                      return vmodel.isListView
                          ? _ListView(
                              charactersList: vmodel.filteredList,
                            )
                          : _GridView(
                              charactersList: vmodel.filteredList,
                            );
                    },
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
